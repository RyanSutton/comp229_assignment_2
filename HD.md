In this level of the project I added a new type of pattern called the decorator pattern. For this decorator pattern there are two main changes that were added.
These include the image of a small sheep inside of the square that represents the shepherd and also expanding the border for the shepherd as he lands on
a jump pad. By using an abstract class of CharacterDecorator which extends the Character class I implemeted the HighlightedCharacterLarge and SheepDecorator.
Where the former is used for when the shepherd is on a jump pad and the latter to display the sheep on top of the shepherd.

There were extra behaviours added at this level which is the RoamGuard behaviour where there is a second sheep that will take a different path around the grid
to guard the grid. This behaviour is the default behaviour of any wolf that specifies it as its behaviour; although once it can see the sheep it will change
it's behaviour to Chase. The same logic applies to the other wolves that use the WatchGuard behaviour that was required for credit level. Furthermore, the
sheep will use the MoveRandom class as its default and then switch to the Chase clas once it can see the shepherd.

As added extras, the purple squares are jump pads which assist with changing the decorator of the shepherd when he is on top of one. This in turn changes
the shepherds behaviour to move two spaces instead of one. There is also a small pen in the lower right of the screen, and if the sheep and shepherd are
both in the pen, then the player wins the game. It is intended that even if the sheep is not being carried by the shepherd and they are both in the pen that
the player can still win the game.

There is only one undesired interaction between behaviours. This occurs when the sheep is being carried by the shepherd and the shepherd is on a jump pad. The
shepherd can move two spaces, and if the player chooses to do so, the sheep will not move those two space and will only move one space in the direction of
the shepherd. This could possibly be fixed by having a BeingCarried behaviour for the sheep where it will be in the exact location of the shepherd no matter
where the shepherd moves to on the grid.