When comparing my implementation of SheepsAndWolves to the example of SimUDuck in the textbook there are many similarities. Firstly the Character class
in my project matches the Duck class. These are both abstract classes that have subtypes. In SheepsAndWolves the subtypes include the classes Sheep, Shepherd 
and Wolf; corresponding to MallardDuck, RedheadDuck, RubberDuck and DecoyDuck. Furthermore the behaviours Chase, MoveRandom, Passive, RoamGuard and WatchGuard in SheepsAndWolves that can be applied to any
character in the project corresponds to QuackBehaviour and FlyBehaviour in SimUDuck.

Within the Shepherd class of SheepsAndWolves there is a method called `carryingSheep()`, this method is used to tell an object of shepherd that its carrying
a sheep and to then respond accordingly by applying a decorator to the object. There are differences visible here when this is compared to the subtypes of
the Duck class. In the SimUDuck project interfaces are used to add extra methods to explicit objects that should not be added to any other subtypes of the same
supertype. Meaning that my implementation could have been improved by implenting a whole new behaviour that displays any character carrying a sheep.
For example there could be the original Passive behaviour and also a new CarryingPassive behaviour, where the latter has the extra method to add the
decorator for carrying the sheep.