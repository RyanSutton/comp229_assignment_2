package main;

import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.List;
import onscreen.*;

public class Stage extends javax.swing.JPanel implements MouseListener, MouseMotionListener {
	public Grid grid;
	public onscreen.Character sheep;
	public onscreen.Character wolf;
	public onscreen.Character wolf2;
	public onscreen.Character wolf3;
	public onscreen.Character shepherd;
	public boolean readyToStep;
	public int stepCounter;
	public boolean shepCarryingSheep = false;
	public boolean wolf2Avail = false;
	public boolean wolf3Avail = false;
	public boolean shepJump = false;
	public int wolfXDir = -1;
	public int wolfYDir = 1;

	Point lastMouseLoc = new Point(0, 0);
	Cell mouseWasIn = null;

	List<MouseObserver> observers = new ArrayList<MouseObserver>();

	public Stage() {
		readyToStep = false;
		grid = new Grid();

		for (int i = 0; i < 20; i++)
			for (int j = 0; j < 20; j++)
				registerMouseObserver(grid.getCell(i, j));

//		shepherd = new Shepherd(this, grid.giveMeRandomCell());
//		sheep = new Sheep(this, grid.giveMeRandomCell());
		shepherd = new Shepherd(this, grid.getCell(16, 16));
		sheep = new Sheep(this, grid.getCell(16, 16));
		// wolf = new Wolf(this, grid.giveMeRandomCell());
		wolf = new Wolf(this, grid.getCell(9, 9));
		wolf2 = new Wolf(this, grid.getCell(9, 0));
		wolf3 = new Wolf(this, grid.getCell(9, 14));
		// wolf = new Wolf(this, grid.getCell(9, 0));

		registerMouseObserver(shepherd);

		addMouseListener(this);
		addMouseMotionListener(this);
	}

	public void paint(Graphics g) {
		draw(g);
	}

	public void draw(Graphics g) {
		grid.draw(g);
		sheep.draw(g);
		wolf.draw(g);
		if (wolf2Avail) {
			wolf2.draw(g);
		}
		if (wolf3Avail) {
			wolf3.draw(g);
		}
		shepherd.draw(g);
		if (result() == -1) {
			g.setColor(Color.BLACK);
			g.setFont(new Font(Font.DIALOG, Font.BOLD, 36));
			g.drawString("Game Over!", 200, 200);
		} else if (result() == 1) {
			g.setColor(Color.yellow);
			g.setFont(new Font(Font.DIALOG, Font.BOLD, 36));
			g.drawString("You Win!", 200, 200);
		}
	}

	public void step() {
		stepCounter++;
		sheep.act();
		wolf.act();
		readyToStep = false;

		if (sheep.getLocation().inRange(shepherd.getLocation())) {
			sheep.setBehaviour(new behaviours.Chase(shepherd));
		}
		if(wolf.getLocation().inRange(sheep.getLocation())){
			wolf.setBehaviour(new behaviours.Chase(sheep));
		}
		
		if (shepherd.getLocation().equals(sheep.getLocation())) {
			shepherd.carryingSheep();
			shepCarryingSheep = true;
		}
		if (stepCounter % 10 == 0) {
			wolf2Avail = true;
		}
		if (stepCounter % 30 == 0) {
			wolf3Avail = true;
		}
		if (wolf2Avail) {
			wolf2.setBehaviour(new behaviours.RoamGuard());
			if(wolf2.getLocation().inRange(sheep.getLocation())){
				wolf2.setBehaviour(new behaviours.Chase(sheep));
			}
			wolf2.act();
		}
		if (wolf3Avail) {
			if(wolf3.getLocation().inRange(sheep.getLocation())){
				wolf3.setBehaviour(new behaviours.Chase(sheep));
			}
			wolf3.act();
		}
		for(int i = 0; i<20; i++){
			for(int j = 0; j<20; j++){
				if((i+1)%5 == 0 && (j+1)%5 == 0){
					if(shepherd.getLocation().equals(grid.getCell(i, j))){
						shepJump = true;
					}
				}
			}
		}		
	}

	public void registerMouseObserver(MouseObserver mo) {
		observers.add(mo);
	}

	public Cell oneCellCloserTo(Cell from, Cell to) {
		int xdiff = to.x - from.x;
		int ydiff = to.y - from.y;
		return grid.getCell(from.x + Integer.signum(xdiff), from.y + Integer.signum(ydiff));
	}

	// implementation of MouseListener and MouseMotionListener
	public void mouseClicked(MouseEvent e) {
		if (shepherd.getBounds().contains(e.getPoint())) {
			shepherd.mouseClicked(e);
		}
	}

	public void mouseEntered(MouseEvent e) {
	}

	public void mouseExited(MouseEvent e) {
	}

	public void mousePressed(MouseEvent e) {
	}

	public void mouseReleased(MouseEvent e) {
	}

	public void mouseDragged(MouseEvent e) {
	}

	public void mouseMoved(MouseEvent e) {
		for (MouseObserver mo : observers) {
			Rectangle bounds = mo.getBounds();
			if (bounds.contains(e.getPoint())) {
				mo.mouseEntered(e);
			} else if (bounds.contains(lastMouseLoc)) {
				mo.mouseLeft(e);
			}
		}
		lastMouseLoc = e.getPoint();
	}

	public int result() {
		// if (shepherd.getLocation().equals(wolf.getLocation())){
		// return -1;
		// }else
		if (wolf.getLocation().equals(sheep.getLocation()) || wolf2.getLocation().equals(sheep.getLocation()) || wolf3.getLocation().equals(sheep.getLocation())) {
			return -1;
		} else if ((sheep.getLocation().x > 16 && sheep.getLocation().y > 16)
				&& (shepherd.getLocation().x > 16 && shepherd.getLocation().y > 16)) {
			return 1;
		} else {
			return 0;
		}
	}
}
