package onscreen;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;

public class SheepDecorator extends CharacterDecorator {
	Rectangle miniSheep;

	public SheepDecorator(Character c) {
		super(c);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void draw(Graphics g) {
		character.draw(g);
		g.setColor(Color.WHITE);
		g.fillRect(character.getLocation().getTopLeft().x+15, character.getLocation().getTopLeft().y+20, 16, 7);
		g.fillRect(character.getLocation().getTopLeft().x+12, character.getLocation().getTopLeft().y+20, 16, 3);
		g.fillRect(character.getLocation().getTopLeft().x+17, character.getLocation().getTopLeft().y+27, 2, 3);
		g.fillRect(character.getLocation().getTopLeft().x+20, character.getLocation().getTopLeft().y+27, 2, 3);
		g.fillRect(character.getLocation().getTopLeft().x+26, character.getLocation().getTopLeft().y+27, 2, 3);
		g.fillRect(character.getLocation().getTopLeft().x+29, character.getLocation().getTopLeft().y+27, 2, 3);
	}

}
