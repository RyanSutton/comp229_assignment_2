package behaviours;

import main.*;
import onscreen.*;

public class MoveRandom implements Behaviour{
	
	public Cell execute(Stage stage, Cell location) {
		return stage.oneCellCloserTo(location, stage.grid.giveMeRandomCell());
	}

}
