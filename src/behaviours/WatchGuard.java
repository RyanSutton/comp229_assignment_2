package behaviours;

import main.Stage;
import onscreen.Cell;

public class WatchGuard implements Behaviour {

	int x, y;
	Cell newLoc = new Cell();
	boolean movingLeft = true;
	
	onscreen.Character target;
	  
	public WatchGuard(onscreen.Character target){this.target = target;}

	public Cell execute(Stage stage, Cell location) {
		x = location.x;
		y = location.y;

		if(movingLeft == true){
			newLoc.x = x-1;
			newLoc.y = y;
			if((x-1)<1){
				movingLeft = false;
			}
			return newLoc;
		}
		else{
			newLoc.x = x+1;
			newLoc.y = y;
			if((x+1)>18){
				movingLeft = true;
			}
			return newLoc;
		}
	}
	
}
