package behaviours;

import main.Stage;
import onscreen.Cell;

public class RoamGuard implements Behaviour {

	int x, y;
	Cell newLoc = new Cell();
	boolean movingLeft = true;

	public Cell execute(Stage stage, Cell location) {
		x = location.x;
		y = location.y;
		Cell newLoc = new Cell();
		if(x == 0){
			stage.wolfXDir = 1;
		}
		if(y == 19){
			stage.wolfYDir = -1;
		}
		if(x == 19){
			stage.wolfXDir = -1;
		}
		if(y == 0){
			stage.wolfYDir = 1;
		}
		newLoc.x = x+(1*stage.wolfXDir);
		newLoc.y = y+(1*stage.wolfYDir);
		
		return newLoc;

	}
	
}
